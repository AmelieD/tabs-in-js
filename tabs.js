/*
var show_spoiler_content = document.querySelector('.spoiler button')

show_spoiler_content.addEventListener('click', function() {
    // rendre visible le text
    this.nextElementSibling.classList.add('visible')
    // remove le bouton
    this.parentNode.removeChild(this)
})
*/

/* clique sur onglet : 
    - retirer class active 
    - add class active à l'onglet current 
    - retire la class active sur contenu 
    - add active sur current content
*/       

(function(){
    var afficher_onglet = function(a, animations){
        var li = a.parentNode
        // au chargement de la page on n'a pas d'animations, donc passer ce paramètre dans la fonction sinon au chargement il n'y aura rien dans le contenu
        if (animations === undefined) {
            animations = true
        }
        var div = a.parentNode.parentNode.parentNode
        var active_tab = div.querySelector('.tab-content.active')  // contenu actif
        var a_afficher =  div.querySelector(a.getAttribute('href')) // contenu à afficher

        if (li.classList.contains('active')) {
            return false
        }
    
        // Remove active from active
        div.querySelector('.tabs .active').classList.remove('active')
        // add active for current li
        li.classList.add('active')
    
        // remove active from tab content
        if (animations) {
            active_tab.classList.add('fade')
            active_tab.classList.remove('in')
            // attention a enlever l'écoute sinon doublons
            var transition_end = function() {
                // remove fade and active 
                this.classList.remove('fade')
                this.classList.remove('active')
                // add active on tab-content corresponding to clicked tab
                a_afficher.classList.add('active')
                a_afficher.classList.add('fade')
                a_afficher.offsetwidth
                a_afficher.classList.add('in')
                // écoute à enlever
                active_tab.removeEventListener('transitionend', transition_end)
            }
            active_tab.addEventListener('transitionend', transition_end) 
        } else {
            a_afficher.classList.add('active')
            active_tab.classList.remove('active')
        }
        
    }
    
    var tabs = document.querySelectorAll('.tabs a')
    for(var i = 0; i < tabs.length; i++) {
        tabs[i].addEventListener('click', function(e){
            afficher_onglet(this, true)
        })
    }
    
    /*
    Récupération du hash 
    Ajouter class active sur le lien href="hash"
    Retirer class active sur autres onglets 
    Afficher / masquer les contenus
    */ 

    // qaund retour arriere, animation se fait bien 
    var hash_change = function(e) {
        var hash = window.location.hash
        var a = document.querySelector('a[href="' + hash + '"]')
        if (a!== null && !a.classList.contains('active')){
            afficher_onglet(a, e!== undefined)
        }
    }

    //changer le hash dans le navigateur
    window.addEventListener('hashchange', hash_change)
    hash_change()
})()





